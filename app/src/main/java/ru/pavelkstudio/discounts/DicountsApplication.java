package ru.pavelkstudio.discounts;

import android.app.Application;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.pavelkstudio.discounts.Retrofit.RetrofitServices;

public class DicountsApplication extends Application {
    static Retrofit retrofit;
    static RetrofitServices serverApi;

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.api_url))
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder().retryOnConnectionFailure(true).build())
                .build();

        serverApi = retrofit.create(RetrofitServices.class);
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static RetrofitServices getServerApi() {
        return serverApi;
    }

}
