package ru.pavelkstudio.discounts.adapters;


import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ru.pavelkstudio.discounts.Model.Coupon;
import ru.pavelkstudio.discounts.R;
import ru.pavelkstudio.discounts.ViewHolderYandexAds;

public class CouponsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<CuponAdapterItem> mList = new ArrayList<>();
    private final Context mContext;
    private final CuponsAdapterListener listener;

    public CouponsAdapter(Context context, CuponsAdapterListener listener) {
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        CuponAdapterItem item = mList.get(position);
        if(item.type == CuponAdapterItemType.cupon)
            return 0;
        else
            return 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0)
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cupon_item, parent, false));
        else
            return new ViewHolderYandexAds(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_yandexads, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holdr, final int position) {
        if(holdr instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) holdr;
            Coupon element = mList.get(position).element;
            if(!TextUtils.isEmpty(element.getTitle()))
                holder.name.setText(Html.fromHtml(element.getTitle()));
            if(!TextUtils.isEmpty(element.getDescr()))
                holder.subname.setText(Html.fromHtml(element.getDescr()));
            String imageUrl = element.getImageUrl();
            if (imageUrl.equals("/html/img/coupons/sale.png"))
                imageUrl = "https://berikod.ru" + imageUrl;
            if(!imageUrl.equals(""))
                holder.image.setVisibility(View.VISIBLE);

            String addButtonText = element.getValid();

            if(element.getType()!= null && element.getType().equals("coupon")) {
                setTextToButton("Открыть купон", Html.fromHtml(addButtonText).toString(), holder);
            } else {
                setTextToButton("Открыть акцию", Html.fromHtml(addButtonText).toString(), holder);
            }
            Glide.with(mContext).asBitmap().load(imageUrl).fitCenter().into(holder.image);

            holder.actionButton.setOnClickListener(v -> {
                if (listener != null)
                    listener.onActionClick(element);
            });

            holder.share.setOnClickListener(v -> {
                if(listener != null)
                    listener.onShareClick(element);
            });
        }
        if(holdr instanceof ViewHolderYandexAds) {
            ((ViewHolderYandexAds)holdr).loadNew("R-M-3004061-1");
        }
    }
    private void setTextToButton (String str, String str2, ViewHolder holder) {

        SpannableString spannedString = new SpannableString( str + "\n" + str2);
        spannedString.setSpan(new RelativeSizeSpan(0.9f), str.length(),str.length() + str2.length()+1, 0); // set size
        spannedString.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.white80)), str.length(),str.length() + str2.length()+1, 0);// set color


        holder.actionButton.setText(spannedString);
    }
    public void setData(List<Coupon> elements) {
        mList.clear();
        int i = 0;
        if (elements != null) {
            for (Coupon element : elements) {
                if (i == 1) {
                    mList.add(new CuponAdapterItem(element, CuponAdapterItemType.promo));
                    i++;
                }
                if (i != 4) {
                    mList.add(new CuponAdapterItem(element, CuponAdapterItemType.cupon));
                    i++;
                } else {
                    i = 0;
                    mList.add(new CuponAdapterItem(null, CuponAdapterItemType.promo));
                }
            }
        }
        notifyDataSetChanged();
    }
    public interface CuponsAdapterListener {
        void onShareClick(Coupon element);
        void onPromoClick(Coupon element);
        void onItemClick(Coupon element);
        void onActionClick(Coupon element);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image, share;
        TextView name, subname, date;
        View view;

        Button actionButton;
        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            subname = itemView.findViewById(R.id.subname);
            //date = itemView.findViewById(R.id.date);
            actionButton = itemView.findViewById(R.id.actionButton);
            share = itemView.findViewById(R.id.share);
            view = itemView.findViewById(R.id.view);
        }
    }

    public enum CuponAdapterItemType {
        cupon,
        promo
    }

    class CuponAdapterItem {
        Coupon element;
        CuponAdapterItemType type;

        public CuponAdapterItem(Coupon element, CuponAdapterItemType type) {
            this.element = element;
            this.type = type;
        }
    }
}