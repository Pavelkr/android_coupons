package ru.pavelkstudio.discounts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import ru.pavelkstudio.discounts.Model.Category;
import ru.pavelkstudio.discounts.R;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    ArrayList<Category> data;
    private Context mContext;
    private CategoriesAdapterListener listener;

    public CategoriesAdapter(Context context, CategoriesAdapterListener listener) {
        this.mContext = context;
        this.listener = listener;
        this.data = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Category element = data.get(position);

        holder.categoryName.setText(element.getName());

        holder.view.setOnClickListener(v -> {
            if(listener != null)
                listener.onItemClick(element);
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ArrayList<Category> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public interface CategoriesAdapterListener {
        void onItemClick(Category element);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView categoryName;
        View view;

        public ViewHolder(View itemView) {
            super(itemView);

            categoryName = itemView.findViewById(R.id.categoryName);
            view = itemView.findViewById(R.id.view);
        }
    }
}