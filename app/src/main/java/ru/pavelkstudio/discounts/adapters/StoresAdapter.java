package ru.pavelkstudio.discounts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.pavelkstudio.discounts.Model.Shop;
import ru.pavelkstudio.discounts.R;
import ru.pavelkstudio.discounts.ViewHolderYandexAds;

public class StoresAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private List<StoreAdapterItem> mList = new ArrayList();
    private List<StoreAdapterItem> dataFiltered = new ArrayList();
    private Context mContext;
    private StoresAdapterListener listener;

    public StoresAdapter(Context context, StoresAdapterListener listener) {
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        StoreAdapterItem item = dataFiltered.get(position);
        if(item.type == StoreAdapterItemType.store)
            return 0;
        else
            return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0)
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.store_item, parent, false));
        else
            return new ViewHolderYandexAds(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_yandexads, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holdr, final int position) {
        if(holdr instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) holdr;
            Shop element = dataFiltered.get(position).element;

            holder.shopName.setText(element.getName());
            holder.shopDescr.setText(element.getName()); //TODO: Сделать возврат с сервера подстрочника
            Glide.with(mContext).asBitmap().load(element.getImage()).fitCenter().into(holder.image);

            holder.view.setOnClickListener(v -> {
                if (listener != null)
                    listener.onItemClick(element);
            });
        }
        if(holdr instanceof ViewHolderYandexAds) {
            ((ViewHolderYandexAds)holdr).loadNew("R-M-3004061-1");
        }
    }

    public void setData(ArrayList<Shop> elements) {
        mList.clear();
        int i = 0;
        if (elements != null) {
            Map<String, Shop> map = new HashMap<>();
            for (Shop element : elements) {
                if (map.get(element.getName()) == null) {
                    if (i != 5) {
                        i++;
                    } else {
                        i = 0;
                        mList.add(new StoreAdapterItem(null, StoreAdapterItemType.promo));
                    }
                    map.put(element.getName(), element);
                    mList.add(new StoreAdapterItem(element, StoreAdapterItemType.store));
                }
            }
            mList.add(2, new StoreAdapterItem(null, StoreAdapterItemType.promo));

            dataFiltered.addAll(mList);
            notifyDataSetChanged();
        }
    }

    public interface StoresAdapterListener {
        void onItemClick(Shop element);
    }

    @Override
    public int getItemCount() {
        return dataFiltered.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView shopName, shopDescr;
        View view;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.shopImage);
            shopName = itemView.findViewById(R.id.shopName);
            shopDescr = itemView.findViewById(R.id.shopDescr);
            view = itemView.findViewById(R.id.view);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                dataFiltered = (List<StoreAdapterItem>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<StoreAdapterItem> filteredResults;
                if (constraint.length() == 0) {
                    filteredResults = mList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    protected List<StoreAdapterItem> getFilteredResults(String constraint) {
        List<StoreAdapterItem> results = new ArrayList<>();

        for (StoreAdapterItem item : mList) {
            if (item.type == StoreAdapterItemType.store && item.element.getName() != null && item.element.getName().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }

    public class PromoViewHolder extends RecyclerView.ViewHolder{
        public PromoViewHolder(View itemView) {
            super(itemView);
        }
    }

    public enum StoreAdapterItemType {
        store,
        promo
    }

    class StoreAdapterItem {
        Shop element;
        StoreAdapterItemType type;

        public StoreAdapterItem(Shop element, StoreAdapterItemType type) {
            this.element = element;
            this.type = type;
        }
    }
}