package ru.pavelkstudio.discounts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class CouponsActivity extends AppCompatActivity {

    public static final int REQUEST_RESULT = 2312;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static void createActivity(String url, Fragment fragment) {
        Intent intent = new Intent(fragment.getActivity(), CouponsActivity.class);
        intent.putExtra("url", url);
        fragment.startActivityForResult(intent, REQUEST_RESULT);
    }
}