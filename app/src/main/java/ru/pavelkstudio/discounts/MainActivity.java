package ru.pavelkstudio.discounts;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import io.appmetrica.analytics.AppMetrica;
import io.appmetrica.analytics.AppMetricaConfig;
import ru.pavelkstudio.discounts.fragments.AboutFragment;
import ru.pavelkstudio.discounts.fragments.CategoriesFragment;
import ru.pavelkstudio.discounts.fragments.CouponsFragment;
import ru.pavelkstudio.discounts.fragments.StoresFragment;

public class MainActivity extends BaseActivity {

    Fragment activeFragment;
    StoresFragment storesFragment;
    CouponsFragment couponsFragment;
    CategoriesFragment categoriesFragment;
    AboutFragment aboutFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppMetrica.activate(this, AppMetricaConfig.newConfigBuilder("e8d7d7ef-e98a-4385-849c-a1d973c134cf").build());
        setContentView(R.layout.activity_main);

        AppMetrica.reportEvent("appStarted");

        initBottomNav();
        couponsFragment = new CouponsFragment();
        setFragment(couponsFragment);
    }

    private void initBottomNav() {
        BottomNavigationView navigation = findViewById(R.id.bottom_nav);
        navigation.setItemHorizontalTranslationEnabled(false);
        navigation.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.cupons:
                    if (couponsFragment == null)
                        couponsFragment = new CouponsFragment();
                    setFragment(couponsFragment);
                    return true;
                case R.id.stores:
                    if (storesFragment == null)
                        storesFragment = new StoresFragment();
                    setFragment(storesFragment);
                    AppMetrica.reportEvent("shopFragmentOpened");
                    return true;
                case R.id.category:
                    if (categoriesFragment == null)
                        categoriesFragment = new CategoriesFragment();
                    setFragment(categoriesFragment);
                    AppMetrica.reportEvent("categoryFragmentOpened");
                    return true;
                case R.id.about:
                    if (aboutFragment == null)
                        aboutFragment = new AboutFragment();
                    setFragment(aboutFragment);
                    AppMetrica.reportEvent("aboutFragmentOpened");
                    return true;
            }
            return false;
        });
    }

    public void setFragment(Fragment fragment) {
        if (isFinishing() && isDestroyed())
            return;

        FragmentManager fragmentManager = getSupportFragmentManager();
        if(fragment != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.main_fragment_container, fragment);
            ft.commitAllowingStateLoss();
            activeFragment = fragment;
        }
    }
}