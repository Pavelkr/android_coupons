package ru.pavelkstudio.discounts;

import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.yandex.mobile.ads.common.AdRequestError;
import com.yandex.mobile.ads.nativeads.NativeAd;
import com.yandex.mobile.ads.nativeads.NativeAdException;
import com.yandex.mobile.ads.nativeads.NativeAdLoadListener;
import com.yandex.mobile.ads.nativeads.NativeAdLoader;
import com.yandex.mobile.ads.nativeads.NativeAdRequestConfiguration;
import com.yandex.mobile.ads.nativeads.NativeAdView;
import com.yandex.mobile.ads.nativeads.NativeAdViewBinder;

import ru.pavelkstudio.discounts.R;

public class ViewHolderYandexAds extends RecyclerView.ViewHolder {
    private NativeAdView adView;
    private Skeleton skeleton;

    public ViewHolderYandexAds(View view) {
        super(view);
        adView = (NativeAdView) view.findViewById(R.id.native_ad_view);
        skeleton  = view.findViewById(R.id.skeletonLayout);
    }

    public void loadNew(String adUnitId) {
        adView.setVisibility(View.GONE);
        NativeAdLoader nativeAdLoader = new NativeAdLoader(adView.getContext());
        skeleton.setMaskColor(ContextCompat.getColor(adView.getContext(), R.color.skeleton_background));
        skeleton.showSkeleton();
        nativeAdLoader.setNativeAdLoadListener(new NativeAdLoadListener() {
            @Override
            public void onAdLoaded(NativeAd nativeAd) {
                Log.v("ads-yandex", "YandexAds onAdLoaded");
                NativeAdViewBinder nativeAdViewBinder = new NativeAdViewBinder.Builder(adView)
                        .setAgeView(adView.findViewById(R.id.age))
                        .setBodyView(adView.findViewById(R.id.body))
                        .setCallToActionView(adView.findViewById(R.id.call_to_action))
                        .setDomainView(adView.findViewById(R.id.domain))
                        .setFaviconView(adView.findViewById(R.id.favicon))
                        .setFeedbackView(adView.findViewById(R.id.feedback))
                        .setIconView(adView.findViewById(R.id.icon))
                        .setMediaView(adView.findViewById(R.id.media))
                        .setPriceView(adView.findViewById(R.id.price))
                        .setRatingView(adView.findViewById(R.id.rating))
                        .setReviewCountView(adView.findViewById(R.id.review_count))
                        .setSponsoredView(adView.findViewById(R.id.sponsored))
                        .setTitleView(adView.findViewById(R.id.title))
                        .setWarningView(adView.findViewById(R.id.warning))
                        .build();
                try {
                    nativeAd.bindNativeAd(nativeAdViewBinder);
                    skeleton.showOriginal();
                    adView.setVisibility(View.VISIBLE);
                } catch (NativeAdException e) {
                    skeleton.showOriginal();
                    throw new RuntimeException(e);
                }


            }
            @Override
            public void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
                skeleton.showOriginal();
            }
        });

        NativeAdRequestConfiguration nativeAdRequestConfiguration = new NativeAdRequestConfiguration.Builder(adUnitId)
                .setShouldLoadImagesAutomatically(true)
                .build();
        nativeAdLoader.loadAd(nativeAdRequestConfiguration);
    }
}