package ru.pavelkstudio.discounts.Model

class Shop (
    val link: String?,
    val name: String?,
    val image: String?
)
