package ru.pavelkstudio.discounts.Model

import com.google.gson.annotations.SerializedName

/**
 * Описание ответа со списком анекдотов
 */
class ShopResponse (
    @SerializedName("success") val success: String,
    @SerializedName("shops") val shops: ArrayList<Shop>
)