package ru.pavelkstudio.discounts.Model

import android.text.TextUtils

class Coupon (
    val link: String?,
    val title: String?,
    val descr: String?,
    val valid: String?,
    val photo: String?,
    val couponHash: String?,
    val source: String?,
    val type: String?
)
{
    fun getImageUrl() : String {
        return if (!TextUtils.isEmpty(photo)) photo!! else ""
    }
}
