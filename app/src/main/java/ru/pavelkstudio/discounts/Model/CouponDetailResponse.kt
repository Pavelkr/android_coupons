package ru.pavelkstudio.discounts.Model

import com.google.gson.annotations.SerializedName

/**
 * Описание ответа со списком категорий
 */
class CouponDetailResponse (
    @SerializedName("success") val success: Boolean,
    @SerializedName("coupon_data") val couponData: CouponDetail?
)