package ru.pavelkstudio.discounts.Model

import android.text.TextUtils

class CouponDetail (
    val coupon_expired: String?,
    val coupon_promocod: String?,
    val coupon_name: String?,
    val link__promocod: String?,
    val offer_link: String?,
    val coupon_desc: String?,
    val offer_img: String?,
    val coupon_type: String?
)
{
    fun getImageUrl() : String {
        return if (!TextUtils.isEmpty(offer_img)) offer_img!! else ""
    }
}
