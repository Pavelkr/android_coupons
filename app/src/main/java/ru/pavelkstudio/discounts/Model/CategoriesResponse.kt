package ru.pavelkstudio.discounts.Model

import com.google.gson.annotations.SerializedName

/**
 * Описание ответа со списком категорий
 */
class CategoriesResponse (
    @SerializedName("success") val success: Boolean,
    @SerializedName("categories") val categories: ArrayList<Category>
)