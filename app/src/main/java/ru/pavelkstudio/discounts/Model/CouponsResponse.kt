package ru.pavelkstudio.discounts.Model

import com.google.gson.annotations.SerializedName

/**
 * Описание ответа со списком анекдотов
 */
class CouponsResponse (
    @SerializedName("success") val success: Boolean,
    @SerializedName("coupons") val coupons: List<Coupon>
)