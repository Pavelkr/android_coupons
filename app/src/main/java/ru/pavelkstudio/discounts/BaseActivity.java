package ru.pavelkstudio.discounts;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    private ProgressBarHandler progressBarHandler;

    public void showProgressBar() {
        if(progressBarHandler == null)
            progressBarHandler = new ProgressBarHandler(this);
        progressBarHandler.show();
    }

    public void dissmissProgressBar() {
        progressBarHandler.hide();
    }

    public Boolean isProgressBarShowed() {
        return progressBarHandler.iCount > 0;
    }
}

class ProgressBarHandler {
    private RelativeLayout mProgressBarBackground;
    private ProgressBar mProgressBar;
    private Context mContext;
    int iCount = 0;

    public ProgressBarHandler(Context context) {
        mContext = context;

        ViewGroup layout = (ViewGroup) ((Activity) context).findViewById(android.R.id.content).getRootView();

        layout.setClickable(true);
        layout.setFocusable(true);
        layout.setFocusableInTouchMode(true);


        mProgressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleLarge);
        mProgressBar.setIndeterminate(true);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        mProgressBarBackground = new RelativeLayout(context);
        mProgressBarBackground.setGravity(Gravity.CENTER);
        mProgressBarBackground.addView(mProgressBar);
        layout.addView(mProgressBarBackground, params);
        mProgressBarBackground.setBackgroundColor(Color.argb(100,0,0,0));

        mProgressBarBackground.setClickable(true);
        mProgressBarBackground.setFocusable(true);
        mProgressBarBackground.setFocusableInTouchMode(true);
    }

    public void show() {
        if(iCount == 0)
            mProgressBarBackground.setVisibility(View.VISIBLE);
        iCount++;
        Log.v("loaderShow count:", String.valueOf(iCount));
    }

    public void hide() {
        iCount--;
        Log.v("loaderHide count:", String.valueOf(iCount));
        if(iCount == 0)
            mProgressBarBackground.setVisibility(View.GONE);
    }
}