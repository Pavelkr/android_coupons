package ru.pavelkstudio.discounts.Retrofit

import retrofit2.Call
import retrofit2.http.*
import ru.pavelkstudio.discounts.Model.CouponsResponse
import ru.pavelkstudio.discounts.Model.CategoriesResponse
import ru.pavelkstudio.discounts.Model.CouponDetailResponse
import ru.pavelkstudio.discounts.Model.ShopResponse

interface RetrofitServices {
    @GET("getCouponList")
    fun getCouponList(
        @Query("url") url: String? = null,
        @Query("from") from: String? = null
    ): Call<CouponsResponse>

    @GET("getShops")
    fun getShops(
    ): Call<ShopResponse>

    @GET("getCategories")
    fun getCategories(
    ): Call<CategoriesResponse>

    @GET("getCoupon")
    fun getCoupon(
        @Query("hash") hash: String? = null,
        @Query("source") source: String? = null
    ): Call<CouponDetailResponse>
}