package ru.pavelkstudio.discounts.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.jsoup.nodes.Element;

import io.appmetrica.analytics.AppMetrica;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.pavelkstudio.discounts.BaseActivity;
import ru.pavelkstudio.discounts.DicountsApplication;
import ru.pavelkstudio.discounts.Model.CategoriesResponse;
import ru.pavelkstudio.discounts.Model.Category;
import ru.pavelkstudio.discounts.R;
import ru.pavelkstudio.discounts.adapters.CategoriesAdapter;

public class CategoriesFragment extends Fragment {

    RelativeLayout networkErrorPanel;
    CategoriesAdapter adapter;
    public CategoriesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_categories, container, false);
        if (getActivity() != null)
            getActivity().setTitle(getString(R.string.caterories));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerView = root.findViewById(R.id.rv_categories);
        networkErrorPanel = root.findViewById(R.id.networkErrorPanel);
        recyclerView.setLayoutManager(layoutManager);

        SwipeRefreshLayout mSwipeRefreshLayout = root.findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(adapter, mSwipeRefreshLayout, recyclerView);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        if (adapter == null) {
            adapter = new CategoriesAdapter(getActivity(), new CategoriesAdapter.CategoriesAdapterListener() {
                @Override
                public void onItemClick(Category element) {
                    String shopUrl = element.getLink();
                    String storeName = element.getName();
                    StoresDetailBottomSheetFragment bottomSheetFragment = new StoresDetailBottomSheetFragment(shopUrl, storeName, "category");
                    if(getActivity() != null) {
                        bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
                        AppMetrica.reportEvent("categorySelected");
                    }
                }
            });
            loadData(adapter, mSwipeRefreshLayout, recyclerView);
        }
        recyclerView.setAdapter(adapter);

        return root;
    }
    public void loadData(CategoriesAdapter adapter, SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView) {
        ((BaseActivity) CategoriesFragment.this.getActivity()).showProgressBar();
        Call<CategoriesResponse> sApi = DicountsApplication.getServerApi().getCategories();
        sApi.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(final Call<CategoriesResponse> call, final Response<CategoriesResponse> response) {
                if (response.isSuccessful()) {
                    CategoriesResponse categories = response.body();
                    if(categories != null && categories.getCategories() != null && categories.getCategories().size() > 0) {
                        adapter.setData(categories.getCategories());
                        networkErrorPanel.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        networkErrorPanel.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    try {
                        //networkError(response.errorBody().string(), response.code());
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                if(CategoriesFragment.this.getActivity() != null) {
                    ((BaseActivity) CategoriesFragment.this.getActivity()).dissmissProgressBar();
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                ((BaseActivity) CategoriesFragment.this.getActivity()).dissmissProgressBar();
                recyclerView.setVisibility(View.GONE);
                networkErrorPanel.setVisibility(View.VISIBLE);
            }
        });
    }
}