package ru.pavelkstudio.discounts.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Arrays;

import io.appmetrica.analytics.AppMetrica;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.pavelkstudio.discounts.BaseActivity;
import ru.pavelkstudio.discounts.CouponsActivity;
import ru.pavelkstudio.discounts.DicountsApplication;
import ru.pavelkstudio.discounts.GetCodesAsyncTask;
import ru.pavelkstudio.discounts.Model.Coupon;
import ru.pavelkstudio.discounts.Model.CouponsResponse;
import ru.pavelkstudio.discounts.R;
import ru.pavelkstudio.discounts.adapters.CouponsAdapter;

public class StoresDetailBottomSheetFragment extends BottomSheetDialogFragment {
    Elements mList;
    String storeUrl;
    String storeName;
    String from;
    BottomSheetBehavior<View> bottomSheetBehavior;

    public StoresDetailBottomSheetFragment() {}
    public StoresDetailBottomSheetFragment(String storeUrl, String storeName, String from) {
        this.storeUrl = storeUrl;
        this.storeName = storeName;
        this.from = from;
    }
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.stores_detail_bottom_sheet, null);
        dialog.setContentView(view);

        Button storeDetailCloseBtn = dialog.findViewById(R.id.storeDetailCloseBtn);

        assert storeDetailCloseBtn != null;
        storeDetailCloseBtn.setOnClickListener(v -> dialog.cancel());

        bottomSheetBehavior = BottomSheetBehavior.from((View) view.getParent());
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        bottomSheetBehavior.setDraggable(false);
        CoordinatorLayout layout = dialog.findViewById(R.id.bottomSheetLayout);
        layout.setMinimumHeight(Resources.getSystem().getDisplayMetrics().heightPixels);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerView = dialog.findViewById(R.id.rv_couponsDetails);
        recyclerView.setLayoutManager(layoutManager);

        CouponsAdapter adapter = new CouponsAdapter(getActivity(), new CouponsAdapter.CuponsAdapterListener() {
            @Override
            public void onShareClick(Coupon element) {
                String link =  element.getLink();
                if (link.equals("")) {
                    link =  element.getLink();
                }
                String shareText = "Смотри что я нашел: " + link + "\n\nБольше промокодов и акций в приложении: https://play.google.com/store/apps/details?id=ru.pavelkstudio.discounts";
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
                sendIntent.setType("text/html");
                startActivity(Intent.createChooser(sendIntent, null));
                AppMetrica.reportEvent("couponShared");
            }

            @Override
            public void onPromoClick(Coupon element) {
                CouponsActivity.createActivity(element.getLink(), StoresDetailBottomSheetFragment.this);
            }

            @Override
            public void onItemClick(Coupon element) {

            }

            @Override
            public void onActionClick(Coupon element) {
                if (element == null) {
                    return;
                }

                if (element.getType()!= null && element.getType().equals("coupon")) {
                    String couponId = element.getCouponHash();
                    String source = element.getSource();
                    String storeLink = element.getLink();
                    //Toast.makeText(getActivity(), couponId, Toast.LENGTH_SHORT).show();
                    CouponDetailBottomSheetFragment bottomSheetFragment = new CouponDetailBottomSheetFragment(couponId, source);
                    if (getActivity() != null) {
                        bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
                        AppMetrica.reportEvent("couponOpened");
                    }
                } else {
                    String openCouponURL = element.getLink();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(openCouponURL)));
                    AppMetrica.reportEvent("actionPageOpened");
                }
            }
        });
        recyclerView.setAdapter(adapter);
        SwipeRefreshLayout mSwipeRefreshLayout = dialog.findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            loadData(adapter, mSwipeRefreshLayout, storeUrl);
            mSwipeRefreshLayout.setRefreshing(false);
        });
        loadData(adapter, mSwipeRefreshLayout, storeUrl);
        TextView shopName = dialog.findViewById(R.id.storeName);
        shopName.setText(storeName);
    }

    public void loadData(CouponsAdapter adapter, SwipeRefreshLayout swipeRefreshLayout, String storeUrl) {
        ((BaseActivity) StoresDetailBottomSheetFragment.this.getActivity()).showProgressBar();
        Call<CouponsResponse> sApi = DicountsApplication.getServerApi().getCouponList(storeUrl, this.from);
        sApi.enqueue(new Callback<CouponsResponse>() {
            @Override
            public void onResponse(final Call<CouponsResponse> call, final Response<CouponsResponse> response) {
                if (response.isSuccessful()) {
                    CouponsResponse coupons = response.body();
                    if(coupons != null && coupons.getCoupons() != null && coupons.getCoupons().size() > 0) {
                        adapter.setData(coupons.getCoupons());
                    }
                }
                else
                {
                    try {
                        //networkError(response.errorBody().string(), response.code());
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                if(StoresDetailBottomSheetFragment.this.getActivity() != null) {
                    ((BaseActivity) StoresDetailBottomSheetFragment.this.getActivity()).dissmissProgressBar();
                }
            }

            @Override
            public void onFailure(Call<CouponsResponse> call, Throwable t) {
                ((BaseActivity) StoresDetailBottomSheetFragment.this.getActivity()).dissmissProgressBar();
            }
        });
    }
}
