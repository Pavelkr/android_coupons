package ru.pavelkstudio.discounts.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import io.appmetrica.analytics.AppMetrica;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.pavelkstudio.discounts.BaseActivity;
import ru.pavelkstudio.discounts.DicountsApplication;
import ru.pavelkstudio.discounts.GetCodesAsyncTask;
import ru.pavelkstudio.discounts.Model.CategoriesResponse;
import ru.pavelkstudio.discounts.Model.Shop;
import ru.pavelkstudio.discounts.Model.ShopResponse;
import ru.pavelkstudio.discounts.R;
import ru.pavelkstudio.discounts.StoresActivity;
import ru.pavelkstudio.discounts.adapters.CouponsAdapter;
import ru.pavelkstudio.discounts.adapters.StoresAdapter;

public class StoresFragment extends Fragment {

    EditText searchEdit;
    ImageView search;
    ImageView clear;
    RelativeLayout noResultPanel, networkErrorPanel, searchView;
    StoresAdapter adapter;

    public StoresFragment() { }

    public static StoresFragment newInstance() {
        StoresFragment fragment = new StoresFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_stores, container, false);
        if (getActivity() != null)
            getActivity().setTitle(getString(R.string.shops));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerView = root.findViewById(R.id.rv_stores);
        recyclerView.setLayoutManager(layoutManager);

        SwipeRefreshLayout mSwipeRefreshLayout = root.findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(adapter, mSwipeRefreshLayout, recyclerView);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        searchEdit = root.findViewById(R.id.search_edit);
        searchView = root.findViewById(R.id.search_view);
        search = root.findViewById(R.id.search);
        clear = root.findViewById(R.id.clear);
        noResultPanel = root.findViewById(R.id.noResultPanel);
        networkErrorPanel = root.findViewById(R.id.networkErrorPanel);

        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String searchStr = searchEdit.getText().toString();
                adapter.getFilter().filter(searchEdit.getText());
                clear.setVisibility(TextUtils.isEmpty(searchStr) ? View.GONE : View.VISIBLE);
                if (adapter.getItemCount() == 0 && !TextUtils.isEmpty(searchStr)) {
                    noResultPanel.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    noResultPanel.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                AppMetrica.reportEvent("searchStarted");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        search.setOnClickListener(view -> startSearch(true, view));
        clear.setOnClickListener(view -> startSearch(false, view));
        if (adapter == null) {
            adapter = new StoresAdapter(getActivity(), new StoresAdapter.StoresAdapterListener() {

                @Override
                public void onItemClick(Shop element) {
                    String shopUrl = element.getLink();
                    String storeName = element.getName();
                    StoresDetailBottomSheetFragment bottomSheetFragment = new StoresDetailBottomSheetFragment(shopUrl, storeName, "shop");
                    if (getActivity() != null) {
                        bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
                        AppMetrica.reportEvent("shopSelected");
                    }
                }
            });
            loadData(adapter, mSwipeRefreshLayout, recyclerView);
        }
        recyclerView.setAdapter(adapter);

        return root;
    }

    private void startSearch(boolean search, View view) {
        if (getActivity() != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (!search) {
                searchEdit.setText("");
                searchEdit.clearFocus();
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            } else {
                searchEdit.requestFocus();
                inputManager.showSoftInput(searchEdit, android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT);
            }
        }
    }

    public void loadData(StoresAdapter adapter, SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView) {
        ((BaseActivity) StoresFragment.this.getActivity()).showProgressBar();
        Call<ShopResponse> sApi = DicountsApplication.getServerApi().getShops();
        sApi.enqueue(new Callback<ShopResponse>() {
            @Override
            public void onResponse(final Call<ShopResponse> call, final Response<ShopResponse> response) {
                if (response.isSuccessful()) {
                    ShopResponse shops = response.body();
                    if(shops != null && shops.getShops() != null && shops.getShops().size() > 0) {
                        adapter.setData(shops.getShops());
                        networkErrorPanel.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        searchView.setVisibility(View.VISIBLE);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        networkErrorPanel.setVisibility(View.VISIBLE);
                        searchView.setVisibility(View.GONE);
                    }
                }
                else
                {
                    try {
                        //networkError(response.errorBody().string(), response.code());
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                if(StoresFragment.this.getActivity() != null) {
                    ((BaseActivity) StoresFragment.this.getActivity()).dissmissProgressBar();
                }
            }

            @Override
            public void onFailure(Call<ShopResponse> call, Throwable t) {
                ((BaseActivity) StoresFragment.this.getActivity()).dissmissProgressBar();
                recyclerView.setVisibility(View.GONE);
                networkErrorPanel.setVisibility(View.VISIBLE);
                searchView.setVisibility(View.GONE);
            }
        });
    }
}