package ru.pavelkstudio.discounts.fragments;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.appmetrica.analytics.AppMetrica;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.pavelkstudio.discounts.BaseActivity;
import ru.pavelkstudio.discounts.DicountsApplication;
import ru.pavelkstudio.discounts.Model.CouponDetailResponse;
import ru.pavelkstudio.discounts.Model.CouponsResponse;
import ru.pavelkstudio.discounts.R;

public class CouponDetailBottomSheetFragment extends BottomSheetDialogFragment {
    String couponId;
    String source;
    BottomSheetDialog dialog;
    BottomSheetBehavior<View> bottomSheetBehavior;
    View rootView;
    Animation animBounce, animFadeIn;
    public CouponDetailBottomSheetFragment() {}
    public CouponDetailBottomSheetFragment(String couponId, String source) {
        this.couponId = couponId;
        this.source = source;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            postRequest(couponId, source);
        } catch (IOException e) {
            e.printStackTrace();
        }
        rootView = inflater.inflate(R.layout.coupon_detail_bottom_sheet, container, false);
        animBounce = AnimationUtils.loadAnimation(getContext(), R.anim.bounce);
        animFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);

        ImageView loader = rootView.findViewById(R.id.imageLoader);
        loader.startAnimation(animBounce);

        ImageView copy = rootView.findViewById(R.id.couponCopy);
        TextView couponCode = rootView.findViewById(R.id.couponCode);
        copy.setOnClickListener(v -> {
            if (getActivity() != null) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", couponCode.getText());
                clipboard.setPrimaryClip(clip);

                copy.startAnimation(animFadeIn);
                Toast.makeText(getContext(), "Скопировано!", Toast.LENGTH_SHORT).show();
                AppMetrica.reportEvent("couponCopied");
            }
        });
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    void postRequest(String hash, String source) throws IOException {
        Call<CouponDetailResponse> sApi = DicountsApplication.getServerApi().getCoupon(hash, source);
        sApi.enqueue(new Callback<CouponDetailResponse>() {
            @Override
            public void onResponse(final retrofit2.Call<CouponDetailResponse> call, final Response<CouponDetailResponse> response) {
                if (response.isSuccessful()) {
                    CouponDetailResponse couponData = response.body();
                    if(couponData != null && couponData.getCouponData() != null) {

                        TextView couponHeader = rootView.findViewById(R.id.couponHeader);
                        couponHeader.setText(couponData.getCouponData().getCoupon_name());

                        TextView couponExpired = rootView.findViewById(R.id.couponExpired);
                        couponExpired.setText(couponData.getCouponData().getCoupon_expired());

                        TextView couponCode = rootView.findViewById(R.id.couponCode);
                        couponCode.setText(couponData.getCouponData().getCoupon_promocod());

                        FrameLayout goToShop = rootView.findViewById(R.id.goToShop);

                        String link__promocod = couponData.getCouponData().getLink__promocod();

                        goToShop.setOnClickListener(v -> {
                            //Если это акция, то открываем веб вью
                            if (!link__promocod.equals("")) {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link__promocod));
                                getContext().startActivity(browserIntent);
                            }
                        });

                        rootView.findViewById(R.id.loader).setVisibility(View.GONE);
                        rootView.findViewById(R.id.couponData).setVisibility(View.VISIBLE);
                    } else {
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    CouponDetailBottomSheetFragment.this.dismiss();
                                    Toast.makeText(getContext(), "Ой, что-то пошло не так, попробуйте еще раз", Toast.LENGTH_SHORT).show();
                                }
                            });
                            call.cancel();
                        }
                    }
                }
                else
                {
                    try {
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    CouponDetailBottomSheetFragment.this.dismiss();
                                    Toast.makeText(getContext(), "Ой, что-то пошло не так, попробуйте еще раз", Toast.LENGTH_SHORT).show();
                                }
                            });
                            call.cancel();
                        }
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CouponDetailResponse> call, Throwable t) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            CouponDetailBottomSheetFragment.this.dismiss();
                            Toast.makeText(getContext(), "Ой, что-то пошло не так, попробуйте еще раз", Toast.LENGTH_SHORT).show();
                        }
                    });
                    call.cancel();
                }
            }
        });
    }
}
