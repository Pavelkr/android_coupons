package ru.pavelkstudio.discounts.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import io.appmetrica.analytics.AppMetrica;
import ru.pavelkstudio.discounts.BuildConfig;
import ru.pavelkstudio.discounts.R;

public class AboutFragment extends Fragment {

    public AboutFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_about, container, false);

        if (getActivity() != null)
            getActivity().setTitle(getString(R.string.about));

        TextView appVersion = root.findViewById(R.id.appVersion);
        Button shareAppButton = root.findViewById(R.id.shareAppButton);

        appVersion.setText("Версия приложения: " + BuildConfig.VERSION_NAME);
        shareAppButton.setOnClickListener(v -> {
            String shareText = "Рекомендую скачать приложение: https://play.google.com/store/apps/details?id=ru.pavelkstudio.discounts \n\nВ нем можно найти множество купонов и акций популярных магазинов!";

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
            sendIntent.setType("text/html");
            startActivity(Intent.createChooser(sendIntent, null));
            AppMetrica.reportEvent("appShared");
        });

        TextView feedbackTextView = root.findViewById(R.id.sendEmailText);
        String feedbackText = "Если вы хотите разместить свои промокоды в нашем приложении или вы столкнулись с проблемами в работе приложения вы можете напиcать нам на <a href='mailto:feedback@anecdote.site?subject=Обращение по поводу приложения анекдоты'>электронную почту</a>";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            feedbackTextView.setText(Html.fromHtml(feedbackText, Html.FROM_HTML_MODE_COMPACT));
        } else {
            feedbackTextView.setText(Html.fromHtml(feedbackText));
        }
        feedbackTextView.setMovementMethod(LinkMovementMethod.getInstance());
        return root;
    }
}