package ru.pavelkstudio.discounts.fragments;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import io.appmetrica.analytics.AppMetrica;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.pavelkstudio.discounts.BaseActivity;
import ru.pavelkstudio.discounts.CouponsActivity;
import ru.pavelkstudio.discounts.DicountsApplication;
import ru.pavelkstudio.discounts.GetCodesAsyncTask;
import ru.pavelkstudio.discounts.Model.CategoriesResponse;
import ru.pavelkstudio.discounts.Model.Coupon;
import ru.pavelkstudio.discounts.Model.CouponsResponse;
import ru.pavelkstudio.discounts.R;
import ru.pavelkstudio.discounts.adapters.CouponsAdapter;

public class CouponsFragment extends Fragment {

    public CouponsFragment() { }
    RelativeLayout networkErrorPanel;
    CouponsAdapter adapter;

    public static CouponsFragment newInstance() {
        CouponsFragment fragment = new CouponsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cupons, container, false);

        if (getActivity() != null)
            getActivity().setTitle(getString(R.string.coupons));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        networkErrorPanel = root.findViewById(R.id.networkErrorPanel);
        RecyclerView recyclerView = root.findViewById(R.id.recyler);
        recyclerView.setLayoutManager(layoutManager);

        SwipeRefreshLayout mSwipeRefreshLayout = root.findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(adapter, mSwipeRefreshLayout, recyclerView);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        if (adapter == null) {
            adapter = new CouponsAdapter(getActivity(), new CouponsAdapter.CuponsAdapterListener() {
                @Override
                public void onShareClick(Coupon element) {
                    String shareText = "Смотри что я нашел: " + element.getLink() + "\n\nБольше промокодов и акций в приложении: https://play.google.com/store/apps/details?id=ru.pavelkstudio.discounts";

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
                    sendIntent.setType("text/html");
                    startActivity(Intent.createChooser(sendIntent, null));
                    AppMetrica.reportEvent("couponShared");
                }

                @Override
                public void onPromoClick(Coupon element) {
                    CouponsActivity.createActivity(element.getLink(), CouponsFragment.this);
                }

                @Override
                public void onItemClick(Coupon element) {

                }

                @Override
                public void onActionClick(Coupon element) {
                    if (element == null) {
                        return;
                    }

                    if (element.getType()!= null && element.getType().equals("coupon")) {
                        String couponId = element.getCouponHash();
                        String source = element.getSource();
                        String storeLink = element.getLink();
                        //Toast.makeText(getActivity(), couponId, Toast.LENGTH_SHORT).show();
                        CouponDetailBottomSheetFragment bottomSheetFragment = new CouponDetailBottomSheetFragment(couponId, source);
                        if (getActivity() != null) {
                            bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
                            AppMetrica.reportEvent("couponOpened");
                        }
                    } else {
                        String openCouponURL = element.getLink();
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(openCouponURL)));
                        AppMetrica.reportEvent("actionPageOpened");
                    }
                }
            });

            loadData(adapter, mSwipeRefreshLayout, recyclerView);
        }

        recyclerView.setAdapter(adapter);
        return root;
    }

    public void loadData(CouponsAdapter adapter, SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView) {

        ((BaseActivity) CouponsFragment.this.getActivity()).showProgressBar();
        Call<CouponsResponse> sApi = DicountsApplication.getServerApi().getCouponList("", "");
        sApi.enqueue(new Callback<CouponsResponse>() {
            @Override
            public void onResponse(final Call<CouponsResponse> call, final Response<CouponsResponse> response) {
                if (response.isSuccessful()) {
                    CouponsResponse coupons = response.body();
                    if(coupons != null && coupons.getCoupons() != null && coupons.getCoupons().size() > 0) {
                        adapter.setData(coupons.getCoupons());
                        networkErrorPanel.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        networkErrorPanel.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    try {
                        //networkError(response.errorBody().string(), response.code());
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                if(CouponsFragment.this.getActivity() != null) {
                    ((BaseActivity) CouponsFragment.this.getActivity()).dissmissProgressBar();
                }
            }

            @Override
            public void onFailure(Call<CouponsResponse> call, Throwable t) {
                ((BaseActivity) CouponsFragment.this.getActivity()).dissmissProgressBar();
                recyclerView.setVisibility(View.GONE);
                networkErrorPanel.setVisibility(View.VISIBLE);
            }
        });
    }
}