package ru.pavelkstudio.discounts;

import android.os.AsyncTask;
import android.text.TextUtils;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class GetCodesAsyncTask extends AsyncTask<Void, String, Elements> {

    GetCodesAsyncTaskListener listener;
    String url, query;
    List<String> notQuery;

    //SwipeRefreshLayout mSwipeRefreshLayout;
    public GetCodesAsyncTask(GetCodesAsyncTaskListener listener, String url, String query, List<String> notQuery) {
        super();
        this.listener = listener;
        this.url = url;
        this.query = query;
        this.notQuery = notQuery;
    }

    @Override
    protected Elements doInBackground(Void... voids) {
        Elements elems = null;
        try {
            Document doc = Jsoup.connect(url).get();
            elems = doc.select(query);

            if (notQuery != null && notQuery.size() > 0) {
                for (String oneNotQuery: notQuery) {
                    elems = elems.not(oneNotQuery);
                }
            }

            return elems;
        } catch (Exception e) {
            return elems;
        }
    }
    @Override
    protected void onPreExecute() {
        listener.onStartLoading();
    }
    @Override
    protected void onPostExecute(Elements elements) {
        super.onPostExecute(elements);
        listener.onResult(elements);
    }
    @Override
    protected void onCancelled() {
        super.onCancelled();
        listener.onCancelled();
    }

    public interface GetCodesAsyncTaskListener {
        void onStartLoading();
        void onResult(Elements elements);
        void onCancelled();
    }
}

